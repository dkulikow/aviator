package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var nextRegex = regexp.MustCompile(`.*next`)

type pos struct {
	X int
	Y int
	A float64
}

func nextPath(current pos, size pos, loop bool) []pos {
	up := int(math.Pow(-1, float64(rand.Intn(2))))
	down := int(math.Pow(-1, float64(rand.Intn(2))))
	num := rand.Intn(40) + 10
	next := make([]pos, num)
	next[0] = pos{X: current.X, Y: current.Y, A: 0.0}
	for i := 1; i < num; i++ {
		next[i].Y = next[i-1].Y + (1 * up)
		next[i].X = next[i-1].X + (1 * down)
		if loop && rand.Intn(100) == 1 {
			next[i].A = 360.0
		} else {
			next[i].A = 0.0
		}
	}
	return next
}

func nextRequest(w http.ResponseWriter, r *http.Request) (pos, pos) {
	x, _ := strconv.Atoi(r.URL.Query()["x"][0])
	y, _ := strconv.Atoi(r.URL.Query()["y"][0])
	width, _ := strconv.Atoi(r.URL.Query()["w"][0])
	height, _ := strconv.Atoi(r.URL.Query()["h"][0])

	return pos{X: x, Y: y}, pos{X: width, Y: height}
}

func handleNext(w http.ResponseWriter, r *http.Request) {
	current, size := nextRequest(w, r)
	nextPath := nextPath(current, size, false)
	resp, _ := json.Marshal(nextPath)
	fmt.Fprintf(w, string(resp))
}

func handle(w http.ResponseWriter, r *http.Request) {

	if nextRegex.MatchString(r.URL.Path) {
		handleNext(w, r)
		log.Printf("next %v", r.URL.Path)
	} else {
		log.Printf("load file %v", r.URL.Path)
		path := strings.Split(r.URL.Path, "/")
		http.ServeFile(w, r, path[len(path)-1])
	}
}

func main() {
	http.HandleFunc("/", handle)
	http.ListenAndServe(":80", nil)
}
